#/bin/env bash

do_composer_install() {
  echo "Running composer install..."

  if [ -z $DEV_MODE ]; then
    COMPOSER_DEV_FLAG="--no-dev"
  fi

  echo "composer install $COMPOSER_DEV_FLAG" | su - $DEPLOY_USER --shell=/bin/bash
}

if ! grep $DEPLOY_USER /etc/passwd > /dev/null; then
  echo "Creating deployment user..."

    if useradd --home $DEPLOY_USER_HOME --create-home --shell /bin/bash --groups www-data $DEPLOY_USER; then
      echo "Created deployment user $DEPLOY_USER"
    else
      echo "Could NOT create deployment user $DEPLOY_USER"
    fi

    cat >> $DEPLOY_USER_HOME/.profile <<EOS
export DEPLOY_USER=$DEPLOY_USER
export DEPLOY_USER_HOME=$DEPLOY_USER_HOME
export WORKDIR=$WORKDIR
export SERVER_NAME=$SERVER_NAME
export GIT_REMOTE=$GIT_REMOTE
export PROJECT_DIR=$PROJECT_DIR

umask 002
cd $WORKDIR
EOS

    # Proceed only if the above succeeded
    [ $? = 0 ] \
    && mkdir $DEPLOY_USER_HOME/.ssh \
    && chmod 700 $DEPLOY_USER_HOME/.ssh \
    && chown $DEPLOY_USER:www-data $WORKDIR \
    && ENV_CREATED=1

    if [ $ENV_CREATED ]; then
      echo "Created environment for $DEPLOY_USER"
    else
      echo "Could NOT create environment for $DEPLOY_USER"
      exit 1
    fi
else
  echo "User $DEPLOY_USER already exists"
fi

# Deploy the web app if the git remote was given
if [ $GIT_REMOTE ]; then
  if [ -e $SSHDIR/id_rsa ]; then
    if ! [ -e $DEPLOY_USER_HOME/.ssh/id_rsa ]; then
        echo "Copying SSH key files..."
        cp $SSHDIR/id_rsa* $DEPLOY_USER_HOME/.ssh
        chmod 600 $DEPLOY_USER_HOME/.ssh/*
        chown -R $DEPLOY_USER: $DEPLOY_USER_HOME/.ssh
    else
      echo "Found $DEPLOY_USER_HOME/.ssh/id_rsa"
    fi
  else
    echo "SSH key files not found"
    exit 1
  fi

  # Configure git
  echo "git config pull.rebase false" | su - $DEPLOY_USER --shell=/bin/bash

  if [ -z "$(ls "$WORKDIR")" ]; then
   echo "Deploying web app at $GIT_REMOTE to $WORKDIR"

   echo "ssh-keyscan $GIT_DOMAIN >> ~/.ssh/known_hosts \
     && git clone $GIT_REMOTE $WORKDIR \
     && cd $WORKDIR \
     && git config --global user.name \"$DEPLOY_USER_NAME\" \
     && git config --global user.email $DEPLOY_USER_EMAIL" | su - $DEPLOY_USER --shell=/bin/bash \
     && CODE_DEPLOYED=1

     if ! [ $CODE_DEPLOYED ]; then
       echo "Could NOT deploy code."
       exit 1
     fi
 else
   echo "$WORKDIR contains files"
 fi
fi

echo "Checking for .env..."
 if ! [ -e "$WORKDIR"/.env ]; then
  if ! [ -e "$PROJECTDIR"/.env ]; then
      echo "Could not find .env. Make sure it is in the project folder before building the image"
      echo "Otherwise, make sure it is $WORKDIR before continuing to setup and run the app"
      exit 1
  fi

  if ! cp "$PROJECTDIR"/.env "$WORKDIR"; then
    echo "Could not copy $PROJECTDIR/.env to $WORKDIR"
  fi

  chown $DEPLOY_USER:www-data "$WORKDIR"/.env
else
  echo "Found $WORKDIR/.env"
fi

# Copy any .crt or .key files to $DEPLOY_USER_HOME/ssl
if ! [ -d $DEPLOY_USER_HOME/ssl ]; then
  mkdir $DEPLOY_USER_HOME/ssl
fi

if ! [ -z "$(ls "$PROJECTDIR"/*.key)" ]; then
  cp "$PROJECTDIR"/*.key $DEPLOY_USER_HOME/ssl
fi

if ! [ -z "$(ls "$PROJECTDIR"/*.crt)" ]; then
  cp "$PROJECTDIR"/*.crt $DEPLOY_USER_HOME/ssl
fi

chmod o+r $DEPLOY_USER_HOME/ssl/*

# Build the frontend files

if [ -d "$WORKDIR"/resources/js ]; then
  if ! [ $DEV_MODE ]; then
    YARN_BUILD_COMMAND="yarn build"
  fi

  if [ -e "$PROJECTDIR"/js/config.js ]; then
    cp "$PROJECTDIR"/js/config.js "$WORKDIR"/resources/js/config.js
  fi

  if ! [ -e "$WORKDIR"/resources/js/config.js ]; then
    echo "Please make sure $WORKDIR/resources/js/config.js exists before continuing"
    exit 1
  fi

  su - $DEPLOY_USER --shell=/bin/bash -c "cd "$WORKDIR" && yarn install"
  
  if [ "$YARN_BUILD_COMMAND" ]; then
    su - $DEPLOY_USER --shell=/bin/bash -c "cd "$WORKDIR" && $YARN_BUILD_COMMAND" \
    && FRONTEND_DEPLOYED=1

    if ! [ $FRONTEND_DEPLOYED ]; then
      echo "Could not deploy frontend"
    fi
  fi
fi

set-workdir-permissions.sh

do_composer_install

if [ $MAKE_APP_KEY ]; then
  echo "Setting APP_KEY"
  echo "cd $WORKDIR && php artisan key:generate" | su - $DEPLOY_USER --shell=/bin/bash
fi

echo "Cleaning up..."

if [ -e "$PROJECTDIR"/.gitignore ]; then
  rm "$PROJECTDIR"/.gitignore;
fi

php-fpm
