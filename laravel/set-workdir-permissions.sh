#/bin/env bash

echo "Setting permissions for working directory..."

chown -R $DEPLOY_USER:www-data "$WORKDIR" \
     && chmod -R u+rw,g+r,g-w "$WORKDIR"

if [ -d "$WORKDIR"/storage ] && \
    [ -d "$WORKDIR"/bootstrap/cache ]; then
    chown -R $DEPLOY_USER:www-data "$WORKDIR" \
     && chmod -R u+rw,g+r "$WORKDIR" \
     && chmod -R g+ws storage "$WORKDIR"/bootstrap/cache \
     && PERMISSIONS_SET=1

     if ! [ $PERMISSIONS_SET ]; then
       echo "Could not set permissions for $WORKDIR"
       exit 1
     fi
fi