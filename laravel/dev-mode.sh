#!/bin/bash

if [ $DEV_MODE ]; then
    # Xdebug for general debugging
    if ! php -v | grep Xdebug; then
        install-xdebug.sh
    fi

    # This software will allow Laravel Dusk to be ran in the container
    apt --yes install xvfb libgconf-2-4 libfontconfig libnss3

    # Install latest chromium for Laravel Dusk
    apt --yes install chromium
fi