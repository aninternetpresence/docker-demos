# An Internet Presence - Demos - Docker container

Dockerfiles and resources for setting up the Demos site for An Internet Presence.

## Overview
There are several steps involved in creating the image and deploying the container. The basic summary is:

### Create the environment files
#### Laravel
1. **Optional** `laravel/env_conf/environment.conf` to set `servername` and `serveradmin`
1. **Optional** `laravel/ini/*.ini` files for PHP INI configuration
1. **Optional** `laravel/project/.env` for the `.env` file for the project
1. **Optional, but required for volume-based deployment** `laravel/ssh/id_rsa` and `laravel/ssh/id_rsa.pub` for the key pair to be used for
   first cloning the Laravel project and later managing it via SSH connection to the git remote
1. `laravel/ssl/certs/ssl-cert-snakeoil.pem` containing the SSL private key and leaf cert. It should also end with the CA bundle cert if it is a real certificate. 
1. **Development** `laravel/ssl/certs/ssl-cert-snakeoil.crt` which is just the leaf cert if it is a self-signed cert.
1. **Development** `laravel/ssl/certs/ssl-cert-snakeoil.crt` which is  the key file it is a self-signed cert.

#### MariaDB
1. **Optional** `mariadb/conf.d/*.cnf` files for any MariaDB configuration. For example:

```
[client]
port = 3308

[mysqld]
port = 3308
```
## Server Name
If you'd like to run the site on a different hostname, create a `env_conf/environment.conf` file that defines the site name. It only needs
to have contents like the following:

    Define servername demos.aninternetpresence.local

This would make the site available with domain name `demos.aninternetpresence.local`

## Building and Running
You can deploy the web app files in one of two ways. One option is to bind mount the files on the host to the container. This is ideal for a development environment where you will want the source files on the host machine so they are easier to work with. You can also run `artisan`, `composer`, and other commands from the host for convenience.

Another option, which is ideal for production environments, is to deploy the site files in a container volume via a container user dedicated to updating the project via git. In this
setup, you would use the `aip` user in the container to manage the files in the container. This has a home directory but no shell assigned, so you will need to use `su` to initiate a shell session as the user.

If using this option, then before building the containers make sure to copy the `.env` file to `laravel/project` and copy the SSH key pair files that the `aip` user will use to authenticate with git server via SSH to the folder `laravel/ssh` as `id_rsa` and `id_rsa.pub`.

Build the database and web server containers, then run them on forwarded ports. It is recommended to run the database on the same port on the host and container so that Laravel's `artisan` commands can be ran in either host or container environment:

    docker build -t enderandpeter/aip-demos-mariadb .
    docker build -t enderandpeter/aip-demos .

    docker create network aip-network
    docker volume create aip-demos
    docker volume create aip-demos-db

    docker run -P -p 3308:3308 -v aip-demos-db:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=securerootpass -e MYSQL_PASSWORD=securepass --network=aip-network --name aip-demos-db --restart=always -d enderandpeter/aip-demos-mariadb

### Windows
    docker run -p 8880:80 -p 4443:443 -v aip-demos:/var/www/laravel --add-host=demos.aninternetpresence.local:127.0.0.1 -d -e GIT_REMOTE="git@github.com:enderandpeter/aip-demos.git"  --name=aip-demos --network=aip-network --restart=always enderandpeter/aip-demos

__or__

    docker run -p 8880:80 -p 4443:443 -v c:/Users/Spencer/Documents/GitHub/aip-demos/:/var/www/laravel --network=aip-network --add-host=demos.aninternetpresence.local:127.0.0.1 -d --name=aip-demos --restart=always enderandpeter/aip-demos

### Linux and OS X
    docker run -p 8880:80 -p 4443:443 -v aip-demos:/var/www/laravel/ -d -e GIT_REMOTE="git@github.com:enderandpeter/aip-demos.git" --restart=always --network=aip-network --name=aip-demos enderandpeter/aip-demos

__or__

    docker run -p 8880:80 -p 4443:443 -v ~/www/aip-demos/:/var/www/laravel/ --add-host=demos.aninternetpresence.local:127.0.0.1 -d --restart=always --network=aip-network --name=aip-demos enderandpeter/aip-demos

## SSL

Before building the docker image, copy over a `laravel/ssl/certs/ssl-cert-snakeoil.pem` file. This file should consist of the following concatenated content:

* The private SSL key
* The leaf SSL cert
* The certificate authority cert

For a self-signed cert, it will consist of the just the private key and the leaf cert concatenated into a single file.

Please also copy over `laravel/ssl/certs/ssl-cert-snakeoil.key` and `laravel/ssl/certs/ssl-cert-snakeoil.crt`.

## Volume-based deployment

As mentioned, when deploying on production, it is recommended that you use the `aip` user
to deploy the web site files in a container. To do so, make sure you have the SSH key pair
files in `laravel/ssh` that the `aip` user will use to authenticate with the remote. Also, make sure the `.env` file to be used for the site is in `laravel/project`.

When issuing the `docker run` command, set the `GIT_REMOTE` environment variable to the URL of the git remote that has the site files. The presence of this environment variable will trigger downloading the files from that location and setting up the container accordingly. If you want to bind mount files you already have on your host, simply omit this environment variable.

When setting this variable, there are some additional environment variables you can set for other features. You can set `MAKE_APP_KEY` to any value if you want it to run `php artisan key:generate` to make the app key. You can also set `COMPOSER_DEV` to any value in order to omit the `--no-dev` flag on `composer install`. Be sure to set this if deploying a dev environment where you'll want to use Dusk and such.

Here are the environment variables that can be added to the `docker run` command and their effects:

* `GIT_REMOTE` - When set to the URL of a git remote endpoint, the web app will be cloned from this remote. Setting
this variable will allow the following variables to be recognized.
* `GIT_DOMAIN` - The domain of the git remote to be added to the deploy user's `~/.ssh/known_hosts`. Without this
setting, cloning into the Docker volume will more than likely fail.
* `DEPLOY_USER` - The user who will be created in the container to manage deployment of the web app.
* `DEPLOY_USER_HOME` - Specify the deploy user's home directory
* `WORKDIR` - The absolute filesystem path where the web app will be deployed in the container
* `SSHDIR` - The deploy user's SSH keys will be copied to here from `laravel/ssh` and then copied to
`$DEPLOY_USER_HOME/.ssh`
* `PROJECTDIR` - The files in `laravel/project` will be copied from here to `$WORKDIR`
* `COMPOSER_DEV` - If set to a value, the `--no-dev` flag will NOT be used for `composer install`.
Only set this if you are deploying via a Docker volume in a non-production environment.
* `MAKE_APP_KEY` - Create a new Laravel app key for the environment which is used to create
password hashes.

See the Dockerfile for an example of how to issue commands with the `bash` program on
behalf of the `aip` user so that you can administrate the site as this user.

## Xdebug
If you would like to enable Xdebug, run the `/usr/local/bin/xdebug-install.sh` script in the container and then restart the container.

## Email
This container installs `sendmail` and `mailutils` from `apt-get`. In order to send email, you will need to set the requisite environment variables when issuing the `docker run` statement.

* `DEBUG_EMAIL` - The email address that will recieve test messages, such as the one confirming that the container can send email
* `SMTP_SERVER` - The SMTP server handling the email requests
* `SMTP_USER` - The username for the SMTP account
* `SMTP_PASS` - The password for the SMTP account

You would use a command like the following to run a docker container with email service, namely passing the environment variables:

	 docker run -p 80:80 -p 443:443 -v ~/www/aip-demos/:/var/www/laravel/ -v /etc/ssl:/etc/ssl/ -e DEBUG_EMAIL=my@email.com -e SMTP_SERVER=smtp.mailgun.org -e SMTP_USER=username -e SMTP_PASS=password -d --name=aip-demos enderandpeter/aip-demos

All `SMTP` envrionment variables must be supplied to provide proper setup of the service, but the `DEBUG_EMAIL` setting is optional.

After running the container with those environment variables, run `/usr/local/bin/setup-mail.sh` to setup the mail server with the provided credentials.

# Final Notes
This container is based on [php:apache](https://store.docker.com/images/php)
